//require directive tells us to load the express module
const express = require('express');

// creating a server using express
const app = express();

//port
const port = 4000;


//middlewares
app.use(express.json());
	//allows app to read json data
app.use(express.urlencoded({extended: true}));
	//allowes app to read data from forms
	//by default, information received from the url can only be received as string or array
	//with extended: true, this allows to receive information in other data types such as objects


	//listen to the port and returning msg in the terminal.


//mock database
let users = [
			{		
				email: "nezukoKamado@gmail.com",
				username: 'nezuko01',
				password: 'iAmTanjiro',
				isAdmin: false
			},
			{		
				email: "tanjiroKamado@gmail.com",
				username: 'gonpanchiro',
				password: 'letMeOut',
				isAdmin: false
			},
			{		
				email: "Zenitsu@gmail.com",
				username: 'zenitsuSleeps',
				password: 'iNeedNezuko',
				isAdmin: true
			}
];
let loggedUser;

app.get('/',(req,res)=>{
			res.send('Hellow World')
})

//app - server
//get - HTTP method
// '/'-route name or endpoint
// (req, res)- request and response - will handle requests and responses
//res.send- combines writeHead() and end(), used to send response to our client.

app.listen(port, ()=> console.log(`The Server is running at port ${port}`))

/*
mini activity


*/
app.get('/hello',(req,res)=>{
	res.send('Hello from batch 131')
})

// POST Method
app.post('/',(req,res)=>{
	console.log(req.body);
	res.send(`Hello, I am ${req.body.name}, I am ${req.body.age} years old. I am a ${req.body.description}`)
});
// use console.log to check whats inside the request body.

// mini activity
// app.post('/',(req,res)=>{
// 	console.log(req.body);
// 	res.send(`Hello I am ${res.name}, i an ${res.age} years old`)
// });

//register route
app.post('/users/register',(req,res)=>{
	console.log(req.body);

	let newUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	};

	users.push(newUser)
	console.log(users)

	res.send(`User ${req.body.username} has successfully registered.`)
})

//login route

app.post('/users/login',(req,res)=>{
	console.log(req.body);

	let foundUser = users.find((user)=>{

		return user.username === req.body.username && user.password === req.body.password;

	});

	if (foundUser !== undefined){

		let foundUserIndex = users.findIndex((user)=>{

			return user.username === foundUser.username
		});

		foundUser.index = foundUserIndex;

		loggedUser = foundUser;

		console.log(loggedUser)
		res.send(`Thank you for logging in.`)
	}else{
		loggedUser = foundUser;

		res.send(`Login failed, wrong credentials`)
	}
})



// Activity 

// #1 & #2
app.get('/home',(req,res)=>{
	res.send("Hello! this is my simple message")
})

//#3 & #4

app.get('/users',(req,res)=>{
	res.send(users)
})

